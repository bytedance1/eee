//
//  DocumentViewController.h
//  eee
//
//  Created by bytedance on 2021/3/10.
//

#import <UIKit/UIKit.h>

@interface DocumentViewController : UIViewController

@property (strong) UIDocument *document;

@end
