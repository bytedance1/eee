//
//  DocumentBrowserViewController.h
//  eee
//
//  Created by bytedance on 2021/3/10.
//

#import <UIKit/UIKit.h>

@interface DocumentBrowserViewController : UIDocumentBrowserViewController

- (void)presentDocumentAtURL:(NSURL *)documentURL;

@end
